export JAVA_HOME="/opt/jdk-11.0.5+10"

JMETER_HOME="/root/jmeter_big/apache-jmeter-5.2.1"
current_date=`date "+%Y%m%d_%H%M%S"`

threads="2000"
rumpUp="2000" #seconds
duration="3000" #seconds

JVM_ARGS="-Xms15500m -Xmx15500m"

$JMETER_HOME/bin/jmeter -n -t $JMETER_HOME/bin/load_tests_files/InKnow_LoadTests.jmx \
-l $JMETER_HOME/bin/test-results/result-file_$current_date.txt \
-e -o $JMETER_HOME/bin/test-results/result_$current_date \
-Gthreads=$threads \
-GrumpUp=$rumpUp \
-Gduration=$duration \
-R192.168.0.155,192.168.0.156




