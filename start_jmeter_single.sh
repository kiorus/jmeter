export JAVA_HOME="/opt/jdk-11.0.5+10"

JMETER_HOME="/root/jmeter_big/apache-jmeter-5.2.1"
current_date=`date "+%Y%m%d_%H%M%S"`

threads="4000"
rumpUp="4000" #seconds
duration="5400" #seconds

JVM_ARGS="-Xms4g -Xmx16g"

$JMETER_HOME/bin/jmeter -n -t $JMETER_HOME/bin/load_tests_files/InKnow_LoadTests.jmx \
-l $JMETER_HOME/bin/test-results/result-file_$current_date.txt \
-e -o $JMETER_HOME/bin/test-results/result_$current_date \
-Jthreads=$threads \
-JrumpUp=$rumpUp \
-Jduration=$duration




